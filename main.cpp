// Copyright 2019-2021, Collabora, Ltd.
// Copyright 2024 Lubosz Sarnecki <lubosz@gmail.com>
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief A simple, commented, single file OpenXR example
 * @author Christoph Haag <christoph.haag@collabora.com>
 */

#include <cstdio>
#include <vector>
#include <string>

#define GL_GLEXT_PROTOTYPES
#include <GL/glcorearb.h>
#include <GL/glx.h>

#define XR_USE_PLATFORM_XLIB
#define XR_USE_GRAPHICS_API_OPENGL
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>

// we need an identity pose for creating spaces without offsets
static XrPosef identity_pose = {.orientation = {.x = 0, .y = 0, .z = 0, .w = 1.0},
                                .position = {.x = 0, .y = 0, .z = 0}};

#define HAND_LEFT_INDEX 0
#define HAND_RIGHT_INDEX 1
#define HAND_COUNT 2

// Slightly differs from glm::ortho
inline static glm::mat4
create_projection_from_fov(const XrFovf fov, const float near_z, const float far_z)
{
	const float tan_left = tanf(fov.angleLeft);
	const float tan_right = tanf(fov.angleRight);
	const float tan_down = tanf(fov.angleDown);
	const float tan_up = tanf(fov.angleUp);

	const float tan_width = tan_right - tan_left;
	const float tan_height = tan_up - tan_down;

	const float m11 = 2 / tan_width;
	const float m22 = 2 / tan_height;
	const float m33 = -(far_z + near_z) / (far_z - near_z);

	const float m31 = (tan_right + tan_left) / tan_width;
	const float m32 = (tan_up + tan_down) / tan_height;
	const float m43 = -(far_z * (near_z + near_z)) / (far_z - near_z);

	// clang-format off
	const float mat[16] = {
		m11, 0  , 0  ,  0,
		0  , m22, 0  ,  0,
		m31, m32, m33, -1,
		0  , 0  , m43,  0,
	};
	// clang-format on

	return glm::make_mat4(mat);
}

glm::vec3
xrVecToGlm(const XrVector3f& v)
{
	return glm::vec3(v.x, v.y, v.z);
}

glm::quat
xrQuatToGlm(const XrQuaternionf& q)
{
	return glm::quat(q.w, q.x, q.y, q.z);
}

inline static glm::mat4
create_view_matrix(const glm::vec3& translation, const glm::quat& orientation)
{
	glm::mat4 view = glm::translate(translation) * glm::toMat4(orientation);
	return glm::inverse(view);
}

// =============================================================================
// OpenGL rendering code at the end of the file
// =============================================================================
bool
init_sdl_window(Display** xDisplay,
                uint32_t* visualid,
                GLXFBConfig* glxFBConfig,
                GLXDrawable* glxDrawable,
                GLXContext* glxContext,
                int w,
                int h);

int
init_gl(uint32_t view_count,
        uint32_t* swapchain_lengths,
        std::vector<std::vector<GLuint>>& framebuffers,
        GLuint* shader_program_id,
        GLuint* VAO);

void
render_frame(int w,
             int h,
             GLuint shader_program_id,
             GLuint VAO,
             XrTime predictedDisplayTime,
             int view_index,
             XrSpaceLocation* hand_locations,
             const glm::mat4& projection_matrix,
             const glm::mat4& view_matrix,
             GLuint framebuffer,
             GLuint image,
             bool depth_supported,
             GLuint depthbuffer);

// =============================================================================



// true if XrResult is a success code, else print error message and return false
bool
xr_check(XrInstance instance, XrResult result, const char* format, ...)
{
	if (XR_SUCCEEDED(result))
		return true;

	char resultString[XR_MAX_RESULT_STRING_SIZE];
	xrResultToString(instance, result, resultString);

	char formatRes[XR_MAX_RESULT_STRING_SIZE + 1024];
	snprintf(formatRes, XR_MAX_RESULT_STRING_SIZE + 1023, "%s [%s] (%d)\n", format, resultString,
	         result);

	va_list args;
	va_start(args, format);
	vprintf(formatRes, args);
	va_end(args);

	return false;
}



static void
print_instance_properties(XrInstance instance)
{
	XrResult result;
	XrInstanceProperties instance_props = {
	    .type = XR_TYPE_INSTANCE_PROPERTIES,
	    .next = NULL,
	};

	result = xrGetInstanceProperties(instance, &instance_props);
	if (!xr_check(NULL, result, "Failed to get instance info"))
		return;

	printf("Runtime Name: %s\n", instance_props.runtimeName);
	printf("Runtime Version: %d.%d.%d\n", XR_VERSION_MAJOR(instance_props.runtimeVersion),
	       XR_VERSION_MINOR(instance_props.runtimeVersion),
	       XR_VERSION_PATCH(instance_props.runtimeVersion));
}

static void
print_system_properties(XrSystemProperties* system_properties)
{
	printf("System properties for system %lu: \"%s\", vendor ID %d\n", system_properties->systemId,
	       system_properties->systemName, system_properties->vendorId);
	printf("\tMax layers          : %d\n", system_properties->graphicsProperties.maxLayerCount);
	printf("\tMax swapchain height: %d\n",
	       system_properties->graphicsProperties.maxSwapchainImageHeight);
	printf("\tMax swapchain width : %d\n",
	       system_properties->graphicsProperties.maxSwapchainImageWidth);
	printf("\tOrientation Tracking: %d\n", system_properties->trackingProperties.orientationTracking);
	printf("\tPosition Tracking   : %d\n", system_properties->trackingProperties.positionTracking);
}

static void
print_viewconfig_view_info(uint32_t view_count, XrViewConfigurationView* viewconfig_views)
{
	for (uint32_t i = 0; i < view_count; i++) {
		printf("View Configuration View %d:\n", i);
		printf("\tResolution       : Recommended %dx%d, Max: %dx%d\n",
		       viewconfig_views[0].recommendedImageRectWidth,
		       viewconfig_views[0].recommendedImageRectHeight, viewconfig_views[0].maxImageRectWidth,
		       viewconfig_views[0].maxImageRectHeight);
		printf("\tSwapchain Samples: Recommended: %d, Max: %d)\n",
		       viewconfig_views[0].recommendedSwapchainSampleCount,
		       viewconfig_views[0].maxSwapchainSampleCount);
	}
}

// returns the preferred swapchain format if it is supported
// else:
// - if fallback is true, return the first supported format
// - if fallback is false, return -1
static int64_t
get_swapchain_format(XrInstance instance,
                     XrSession session,
                     int64_t preferred_format,
                     bool fallback)
{
	XrResult result;

	uint32_t swapchain_format_count;
	result = xrEnumerateSwapchainFormats(session, 0, &swapchain_format_count, NULL);
	if (!xr_check(instance, result, "Failed to get number of supported swapchain formats"))
		return -1;

	printf("Runtime supports %d swapchain formats\n", swapchain_format_count);
	std::vector<int64_t> swapchain_formats(swapchain_format_count);

	result = xrEnumerateSwapchainFormats(session, swapchain_format_count, &swapchain_format_count,
	                                     swapchain_formats.data());
	if (!xr_check(instance, result, "Failed to enumerate swapchain formats"))
		return -1;

	int64_t chosen_format = fallback ? swapchain_formats[0] : -1;

	for (uint32_t i = 0; i < swapchain_format_count; i++) {
		printf("Supported GL format: %#lx\n", swapchain_formats[i]);
		if (swapchain_formats[i] == preferred_format) {
			chosen_format = swapchain_formats[i];
			printf("Using preferred swapchain format %#lx\n", chosen_format);
			break;
		}
	}
	if (fallback && chosen_format != preferred_format) {
		printf("Falling back to non preferred swapchain format %#lx\n", chosen_format);
	}

	return chosen_format;
}


static void
print_api_layers()
{
	uint32_t count = 0;
	XrResult result = xrEnumerateApiLayerProperties(0, &count, NULL);
	if (!xr_check(NULL, result, "Failed to enumerate api layer count"))
		return;

	if (count == 0)
		return;

	std::vector<XrApiLayerProperties> props(count);

	for (uint32_t i = 0; i < count; i++) {
		props[i].type = XR_TYPE_API_LAYER_PROPERTIES;
		props[i].next = NULL;
	}

	result = xrEnumerateApiLayerProperties(count, &count, props.data());
	if (!xr_check(NULL, result, "Failed to enumerate api layers"))
		return;

	printf("API layers:\n");
	for (uint32_t i = 0; i < count; i++) {
		printf("\t%s v%d: %s\n", props[i].layerName, props[i].layerVersion, props[i].description);
	}
}


// functions belonging to extensions must be loaded with xrGetInstanceProcAddr before use
static PFN_xrGetOpenGLGraphicsRequirementsKHR pfnGetOpenGLGraphicsRequirementsKHR = NULL;
static bool
load_extension_function_pointers(XrInstance instance)
{
	XrResult result =
	    xrGetInstanceProcAddr(instance, "xrGetOpenGLGraphicsRequirementsKHR",
	                          (PFN_xrVoidFunction*)&pfnGetOpenGLGraphicsRequirementsKHR);
	if (!xr_check(instance, result, "Failed to get OpenGL graphics requirements function!"))
		return false;

	return true;
}

class OpenXrContext
{

	// Changing to HANDHELD_DISPLAY or a future form factor may work, but has not been tested.
	XrFormFactor form_factor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

	// Changing the form_factor may require changing the view_type too.
	XrViewConfigurationType view_type = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

	// Typically STAGE for room scale/standing, LOCAL for seated
	XrReferenceSpaceType play_space_type = XR_REFERENCE_SPACE_TYPE_LOCAL;
	XrSpace play_space = XR_NULL_HANDLE;

	// the instance handle can be thought of as the basic connection to the OpenXR runtime
	XrInstance instance = XR_NULL_HANDLE;
	// the system represents an (opaque) set of XR devices in use, managed by the runtime
	XrSystemId system_id = XR_NULL_SYSTEM_ID;
	// the session deals with the renderloop submitting frames to the runtime
	XrSession session = XR_NULL_HANDLE;

	// each graphics API requires the use of a specialized struct
	// The runtime interacts with the OpenGL images (textures) via a Swapchain.
	XrGraphicsBindingOpenGLXlibKHR graphics_binding_gl = {
	    .type = XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR,
	};

	// each physical Display/Eye is described by a view.
	// view_count usually depends on the form_factor / view_type.
	// dynamically allocating all view related structs instead of assuming 2
	// hopefully allows this app to scale easily to different view_counts.
	uint32_t view_count = 0;

	// array of view_count containers for submitting swapchains with rendered VR frames
	std::vector<XrCompositionLayerProjectionView> projection_views;
	// array of view_count views, filled by the runtime with current HMD display pose
	std::vector<XrView> views;

	XrPath hand_paths[HAND_COUNT];

	// array of view_count handles for swapchains.
	// it is possible to use imageRect to render all views to different areas of the
	// same texture, but in this example we use one swapchain per view
	std::vector<XrSwapchain> swapchains;

	// array of view_count ints, storing the length of swapchains
	std::vector<uint32_t> swapchain_lengths;

	// array of view_count array of swapchain_length containers holding an OpenGL texture
	// that is allocated by the runtime
	std::vector<std::vector<XrSwapchainImageOpenGLKHR>> images;

	// depth swapchain equivalent to the VR color swapchains
	std::vector<XrSwapchain> depth_swapchains;
	std::vector<uint32_t> depth_swapchain_lengths;
	std::vector<std::vector<XrSwapchainImageOpenGLKHR>> depth_images;

	std::vector<XrViewConfigurationView> viewconfig_views;


	XrAction hand_pose_action;
	XrSpace hand_pose_spaces[HAND_COUNT];
	XrActionSet gameplay_actionset;
	XrAction grab_action_float;
	XrAction haptic_action;

	struct
	{
		// supporting depth layers is *optional* for runtimes
		bool supported;
		std::vector<XrCompositionLayerDepthInfoKHR> infos;
	} depth;

	struct
	{
		// To render into a texture we need a framebuffer (one per texture to make it easy)
		std::vector<std::vector<GLuint>> framebuffers;

		float near_z = 0.01f;
		float far_z = 100.0f;

		GLuint shader_program_id;
		GLuint VAO;
	} gl_rendering;

	bool quit_mainloop = false;
	bool session_running = false; // to avoid beginning an already running session
	bool run_framecycle = false;  // for some session states skip the frame cycle

	XrSessionState state = XR_SESSION_STATE_UNKNOWN;

public:
	OpenXrContext();
	~OpenXrContext();

	bool
	init();

	bool
	init_instance();

	bool
	init_system();

	bool
	init_views();

	bool
	init_graphics();

	bool
	init_session();

	bool
	init_space();

	bool
	init_swapchains();

	bool
	init_layers();

	bool
	init_actions();

	bool
	iterate();

	bool
	loop();

	void
	poll_sdl();

	bool
	poll_events();
};

OpenXrContext::OpenXrContext() {}

OpenXrContext::~OpenXrContext()
{
	for (uint32_t i = 0; i < view_count; i++) {
		glDeleteFramebuffers(swapchain_lengths[i], gl_rendering.framebuffers[i].data());
	}
	xrDestroyInstance(instance);

	printf("Cleaned up!\n");
}

bool
OpenXrContext::init_instance()
{
	// xrEnumerate*() functions are usually called once with CapacityInput = 0.
	// The function will write the required amount into CountOutput. We then have
	// to allocate an array to hold CountOutput elements and call the function
	// with CountOutput as CapacityInput.
	uint32_t ext_count = 0;
	XrResult result = xrEnumerateInstanceExtensionProperties(NULL, 0, &ext_count, NULL);

	/* TODO: instance null will not be able to convert XrResult to string */
	if (!xr_check(NULL, result, "Failed to enumerate number of extension properties"))
		return false;


	std::vector<XrExtensionProperties> ext_props(ext_count);
	for (uint32_t i = 0; i < ext_count; i++) {
		// we usually have to fill in the type (for validation) and set
		// next to NULL (or a pointer to an extension specific struct)
		ext_props[i].type = XR_TYPE_EXTENSION_PROPERTIES;
		ext_props[i].next = NULL;
	}

	result = xrEnumerateInstanceExtensionProperties(NULL, ext_count, &ext_count, ext_props.data());
	if (!xr_check(NULL, result, "Failed to enumerate extension properties"))
		return false;

	bool opengl_supported = false;

	printf("Runtime supports %d extensions\n", ext_count);
	for (uint32_t i = 0; i < ext_count; i++) {
		printf("\t%s v%d\n", ext_props[i].extensionName, ext_props[i].extensionVersion);
		if (strcmp(XR_KHR_OPENGL_ENABLE_EXTENSION_NAME, ext_props[i].extensionName) == 0) {
			opengl_supported = true;
		}

		if (strcmp(XR_KHR_COMPOSITION_LAYER_DEPTH_EXTENSION_NAME, ext_props[i].extensionName) == 0) {
			depth.supported = true;
		}
	}

	// A graphics extension like OpenGL is required to draw anything in VR
	if (!opengl_supported) {
		printf("Runtime does not support OpenGL extension!\n");
		return false;
	}

	// --- Create XrInstance
	std::vector<const char*> enabled_exts = {XR_KHR_OPENGL_ENABLE_EXTENSION_NAME};
	// same can be done for API layers, but API layers can also be enabled by env var

	XrInstanceCreateInfo instance_create_info = {
	    .type = XR_TYPE_INSTANCE_CREATE_INFO,
	    .next = NULL,
	    .createFlags = 0,
	    .applicationInfo =
	        {
	            .applicationName = "OpenXR OpenGL Example",
	            .applicationVersion = 1,
	            .engineName = "Custom",
	            .engineVersion = 0,
	            .apiVersion = XR_CURRENT_API_VERSION,
	        },
	    .enabledApiLayerCount = 0,
	    .enabledApiLayerNames = NULL,
	    .enabledExtensionCount = (uint32_t)enabled_exts.size(),
	    .enabledExtensionNames = enabled_exts.data(),
	};

	result = xrCreateInstance(&instance_create_info, &instance);
	if (!xr_check(NULL, result, "Failed to create XR instance."))
		return false;

	if (!load_extension_function_pointers(instance))
		return false;

	// Optionally get runtime name and version
	print_instance_properties(instance);

	return true;
}

bool
OpenXrContext::init_system()
{
	XrSystemGetInfo system_get_info = {
	    .type = XR_TYPE_SYSTEM_GET_INFO,
	    .next = NULL,
	    .formFactor = form_factor,
	};

	XrResult result = xrGetSystem(instance, &system_get_info, &system_id);
	if (!xr_check(instance, result, "Failed to get system for HMD form factor."))
		return false;

	printf("Successfully got XrSystem with id %lu for HMD form factor\n", system_id);

	XrSystemProperties system_props = {
	    .type = XR_TYPE_SYSTEM_PROPERTIES,
	    .next = NULL,
	};

	result = xrGetSystemProperties(instance, system_id, &system_props);
	if (!xr_check(instance, result, "Failed to get System properties"))
		return false;

	print_system_properties(&system_props);

	return true;
}

bool
OpenXrContext::init_views()
{

	XrResult result =
	    xrEnumerateViewConfigurationViews(instance, system_id, view_type, 0, &view_count, NULL);
	if (!xr_check(instance, result, "Failed to get view configuration view count!"))
		return false;

	// Do not allocate these every frame to save some resources
	for (uint32_t i = 0; i < view_count; i++) {
		XrView view = {.type = XR_TYPE_VIEW, .next = NULL};
		views.push_back(view);
	}

	// the viewconfiguration views contain information like resolution about each view
	for (uint32_t i = 0; i < view_count; i++) {
		XrViewConfigurationView view = {.type = XR_TYPE_VIEW_CONFIGURATION_VIEW, .next = NULL};
		viewconfig_views.push_back(view);
	}

	result = xrEnumerateViewConfigurationViews(instance, system_id, view_type, view_count,
	                                           &view_count, viewconfig_views.data());
	if (!xr_check(instance, result, "Failed to enumerate view configuration views!"))
		return false;
	print_viewconfig_view_info(view_count, viewconfig_views.data());

	return true;
}

bool
OpenXrContext::init_graphics()
{
	// OpenXR requires checking graphics requirements before creating a session.
	XrGraphicsRequirementsOpenGLKHR opengl_reqs = {.type = XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR,
	                                               .next = NULL};

	// this function pointer was loaded with xrGetInstanceProcAddr
	XrResult result = pfnGetOpenGLGraphicsRequirementsKHR(instance, system_id, &opengl_reqs);
	if (!xr_check(instance, result, "Failed to get OpenGL graphics requirements!"))
		return false;

	/* Checking opengl_reqs.minApiVersionSupported and opengl_reqs.maxApiVersionSupported
	 * is not very useful, compatibility will depend on the OpenGL implementation and the
	 * OpenXR runtime much more than the OpenGL version.
	 * Other APIs have more useful verifiable requirements. */


	// --- Create session
	// create SDL window the size of the left eye & fill GL graphics binding info
	if (!init_sdl_window(&graphics_binding_gl.xDisplay, &graphics_binding_gl.visualid,
	                     &graphics_binding_gl.glxFBConfig, &graphics_binding_gl.glxDrawable,
	                     &graphics_binding_gl.glxContext,
	                     viewconfig_views[0].recommendedImageRectWidth,
	                     viewconfig_views[0].recommendedImageRectHeight)) {

		printf("SDL init failed!\n");
		return false;
	}

	printf("Using OpenGL version: %s\n", glGetString(GL_VERSION));
	printf("Using OpenGL Renderer: %s\n", glGetString(GL_RENDERER));

	return true;
}

bool
OpenXrContext::init_session()
{
	XrSessionCreateInfo session_create_info = {
	    .type = XR_TYPE_SESSION_CREATE_INFO,
	    .next = &graphics_binding_gl,
	    .systemId = system_id,
	};

	XrResult result = xrCreateSession(instance, &session_create_info, &session);
	if (!xr_check(instance, result, "Failed to create session"))
		return false;

	printf("Successfully created a session with OpenGL!\n");

	return true;
}

bool
OpenXrContext::init_space()
{
	/* Many runtimes support at least STAGE and LOCAL but not all do.
	 * Sophisticated apps might check with xrEnumerateReferenceSpaces() if the
	 * chosen one is supported and try another one if not.
	 * Here we will get an error from xrCreateReferenceSpace() and exit. */
	XrReferenceSpaceCreateInfo play_space_create_info = {.type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO,
	                                                     .next = NULL,
	                                                     .referenceSpaceType = play_space_type,
	                                                     .poseInReferenceSpace = identity_pose};

	XrResult result = xrCreateReferenceSpace(session, &play_space_create_info, &play_space);
	if (!xr_check(instance, result, "Failed to create play space!"))
		return false;

	return true;
}

bool
OpenXrContext::init_swapchains()
{
	uint32_t swapchain_format_count;
	XrResult result = xrEnumerateSwapchainFormats(session, 0, &swapchain_format_count, NULL);
	if (!xr_check(instance, result, "Failed to get number of supported swapchain formats"))
		return false;

	printf("Runtime supports %d swapchain formats\n", swapchain_format_count);
	std::vector<int64_t> swapchain_formats(swapchain_format_count);
	result = xrEnumerateSwapchainFormats(session, swapchain_format_count, &swapchain_format_count,
	                                     swapchain_formats.data());
	if (!xr_check(instance, result, "Failed to enumerate swapchain formats"))
		return false;

	// SRGB is usually a better choice than linear
	// a more sophisticated approach would iterate supported swapchain formats and choose from them
	int64_t color_format = get_swapchain_format(instance, session, GL_SRGB8_ALPHA8_EXT, true);

	// GL_DEPTH_COMPONENT16 is a good bet
	// SteamVR 1.16.4 supports GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT32
	// but NOT GL_DEPTH_COMPONENT32F
	int64_t depth_format = get_swapchain_format(instance, session, GL_DEPTH_COMPONENT16, false);
	if (depth_format < 0) {
		printf("Preferred depth format GL_DEPTH_COMPONENT16 not supported, disabling depth\n");
		depth.supported = false;
	}

	// array of view_count handles for swapchains.
	// it is possible to use imageRect to render all views to different areas of the
	// same texture, but in this example we use one swapchain per view
	swapchains = std::vector<XrSwapchain>(view_count);

	// array of view_count ints, storing the length of swapchains
	swapchain_lengths = std::vector<uint32_t>(view_count);

	// array of view_count array of swapchain_length containers holding an OpenGL texture
	// that is allocated by the runtime
	images = std::vector<std::vector<XrSwapchainImageOpenGLKHR>>(view_count);

	// --- Create swapchain for main VR rendering
	{
		// In the frame loop we render into OpenGL textures we receive from the runtime here.
		for (uint32_t i = 0; i < view_count; i++) {
			XrSwapchainCreateInfo swapchain_create_info = {
			    .type = XR_TYPE_SWAPCHAIN_CREATE_INFO,
			    .next = NULL,
			    .createFlags = 0,
			    .usageFlags = XR_SWAPCHAIN_USAGE_SAMPLED_BIT | XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT,
			    .format = color_format,
			    .sampleCount = viewconfig_views[i].recommendedSwapchainSampleCount,
			    .width = viewconfig_views[i].recommendedImageRectWidth,
			    .height = viewconfig_views[i].recommendedImageRectHeight,
			    .faceCount = 1,
			    .arraySize = 1,
			    .mipCount = 1,
			};

			result = xrCreateSwapchain(session, &swapchain_create_info, &swapchains[i]);
			if (!xr_check(instance, result, "Failed to create swapchain %d!", i))
				return false;

			// The runtime controls how many textures we have to be able to render to
			// (e.g. "triple buffering")
			result = xrEnumerateSwapchainImages(swapchains[i], 0, &swapchain_lengths[i], NULL);
			if (!xr_check(instance, result, "Failed to enumerate swapchains"))
				return false;

			images[i] = std::vector<XrSwapchainImageOpenGLKHR>();
			for (uint32_t j = 0; j < swapchain_lengths[i]; j++) {
				XrSwapchainImageOpenGLKHR image = {.type = XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR,
				                                   .next = NULL};
				images[i].push_back(image);
			}
			result =
			    xrEnumerateSwapchainImages(swapchains[i], swapchain_lengths[i], &swapchain_lengths[i],
			                               (XrSwapchainImageBaseHeader*)images[i].data());
			if (!xr_check(instance, result, "Failed to enumerate swapchain images"))
				return false;
		}
	}

	// --- Create swapchain for depth buffers if supported
	if (depth.supported) {
		// depth swapchain equivalent to the VR color swapchains
		depth_swapchains = std::vector<XrSwapchain>(view_count);
		depth_swapchain_lengths = std::vector<uint32_t>(view_count);
		depth_images = std::vector<std::vector<XrSwapchainImageOpenGLKHR>>(view_count);

		for (uint32_t i = 0; i < view_count; i++) {
			XrSwapchainCreateInfo swapchain_create_info = {
			    .type = XR_TYPE_SWAPCHAIN_CREATE_INFO,
			    .next = NULL,
			    .createFlags = 0,
			    .usageFlags = XR_SWAPCHAIN_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
			    .format = depth_format,
			    .sampleCount = viewconfig_views[i].recommendedSwapchainSampleCount,
			    .width = viewconfig_views[i].recommendedImageRectWidth,
			    .height = viewconfig_views[i].recommendedImageRectHeight,
			    .faceCount = 1,
			    .arraySize = 1,
			    .mipCount = 1,
			};

			result = xrCreateSwapchain(session, &swapchain_create_info, &depth_swapchains[i]);
			if (!xr_check(instance, result, "Failed to create swapchain %d!", i))
				return false;

			result =
			    xrEnumerateSwapchainImages(depth_swapchains[i], 0, &depth_swapchain_lengths[i], NULL);
			if (!xr_check(instance, result, "Failed to enumerate swapchains"))
				return false;

			// these are wrappers for the actual OpenGL texture id
			depth_images[i] = std::vector<XrSwapchainImageOpenGLKHR>();
			for (uint32_t j = 0; j < depth_swapchain_lengths[i]; j++) {
				XrSwapchainImageOpenGLKHR image = {.type = XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR,
				                                   .next = NULL};
				depth_images[i].push_back(image);
			}
			result = xrEnumerateSwapchainImages(depth_swapchains[i], depth_swapchain_lengths[i],
			                                    &depth_swapchain_lengths[i],
			                                    (XrSwapchainImageBaseHeader*)depth_images[i].data());
			if (!xr_check(instance, result, "Failed to enumerate swapchain images"))
				return false;
		}
	}

	return true;
}

bool
OpenXrContext::init_layers()
{
	for (uint32_t i = 0; i < view_count; i++) {
		XrCompositionLayerProjectionView projection_view = {
		    .type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW,
		    .next = NULL,
		    .subImage =
		        {
		            .swapchain = swapchains[i],
		            .imageRect =
		                {
		                    .offset =
		                        {
		                            .x = 0,
		                            .y = 0,
		                        },
		                    .extent =
		                        {
		                            .width = (int32_t)viewconfig_views[i].recommendedImageRectWidth,
		                            .height = (int32_t)viewconfig_views[i].recommendedImageRectHeight,
		                        },
		                },
		            .imageArrayIndex = 0,
		        },
		};
		projection_views.push_back(projection_view);

		// projection_views[i].{pose, fov} have to be filled every frame in frame loop
	};


	if (depth.supported) {
		for (uint32_t i = 0; i < view_count; i++) {
			XrCompositionLayerDepthInfoKHR info = {
			    .type = XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR,
			    .next = NULL,
			    .subImage =
			        {
			            .swapchain = depth_swapchains[i],
			            .imageRect =
			                {
			                    .offset =
			                        {
			                            .x = 0,
			                            .y = 0,
			                        },
			                    .extent =
			                        {
			                            .width = (int32_t)viewconfig_views[i].recommendedImageRectWidth,
			                            .height = (int32_t)viewconfig_views[i].recommendedImageRectHeight,
			                        },
			                },
			            .imageArrayIndex = 0,
			        },
			    .minDepth = 0.f,
			    .maxDepth = 1.f,
			    .nearZ = gl_rendering.near_z,
			    .farZ = gl_rendering.far_z,
			};
			depth.infos.push_back(info);
		}
		// depth is chained to projection, not submitted as separate layer
		for (uint32_t i = 0; i < view_count; i++) {
			projection_views[i].next = &depth.infos[i];
		};
	}
	return true;
}

bool
OpenXrContext::init_actions()
{
	xrStringToPath(instance, "/user/hand/left", &hand_paths[HAND_LEFT_INDEX]);
	xrStringToPath(instance, "/user/hand/right", &hand_paths[HAND_RIGHT_INDEX]);

	XrPath select_click_path[HAND_COUNT];
	xrStringToPath(instance, "/user/hand/left/input/select/click",
	               &select_click_path[HAND_LEFT_INDEX]);
	xrStringToPath(instance, "/user/hand/right/input/select/click",
	               &select_click_path[HAND_RIGHT_INDEX]);

	XrPath trigger_value_path[HAND_COUNT];
	xrStringToPath(instance, "/user/hand/left/input/trigger/value",
	               &trigger_value_path[HAND_LEFT_INDEX]);
	xrStringToPath(instance, "/user/hand/right/input/trigger/value",
	               &trigger_value_path[HAND_RIGHT_INDEX]);

	XrPath thumbstick_y_path[HAND_COUNT];
	xrStringToPath(instance, "/user/hand/left/input/thumbstick/y",
	               &thumbstick_y_path[HAND_LEFT_INDEX]);
	xrStringToPath(instance, "/user/hand/right/input/thumbstick/y",
	               &thumbstick_y_path[HAND_RIGHT_INDEX]);

	XrPath grip_pose_path[HAND_COUNT];
	xrStringToPath(instance, "/user/hand/left/input/grip/pose", &grip_pose_path[HAND_LEFT_INDEX]);
	xrStringToPath(instance, "/user/hand/right/input/grip/pose", &grip_pose_path[HAND_RIGHT_INDEX]);

	XrPath haptic_path[HAND_COUNT];
	xrStringToPath(instance, "/user/hand/left/output/haptic", &haptic_path[HAND_LEFT_INDEX]);
	xrStringToPath(instance, "/user/hand/right/output/haptic", &haptic_path[HAND_RIGHT_INDEX]);


	XrActionSetCreateInfo gameplay_actionset_info = {
	    .type = XR_TYPE_ACTION_SET_CREATE_INFO, .next = NULL, .priority = 0};
	strcpy(gameplay_actionset_info.actionSetName, "gameplay_actionset");
	strcpy(gameplay_actionset_info.localizedActionSetName, "Gameplay Actions");


	XrResult result = xrCreateActionSet(instance, &gameplay_actionset_info, &gameplay_actionset);
	if (!xr_check(instance, result, "failed to create actionset"))
		return false;

	{
		XrActionCreateInfo action_info = {
		    .type = XR_TYPE_ACTION_CREATE_INFO,
		    .next = NULL,
		    .actionType = XR_ACTION_TYPE_POSE_INPUT,
		    .countSubactionPaths = HAND_COUNT,
		    .subactionPaths = hand_paths,
		};
		strcpy(action_info.actionName, "handpose");
		strcpy(action_info.localizedActionName, "Hand Pose");

		result = xrCreateAction(gameplay_actionset, &action_info, &hand_pose_action);
		if (!xr_check(instance, result, "failed to create hand pose action"))
			return false;
	}
	// poses can't be queried directly, we need to create a space for each

	for (int hand = 0; hand < HAND_COUNT; hand++) {
		XrActionSpaceCreateInfo action_space_info = {
		    .type = XR_TYPE_ACTION_SPACE_CREATE_INFO,
		    .next = NULL,
		    .action = hand_pose_action,
		    .subactionPath = hand_paths[hand],
		    .poseInActionSpace = identity_pose,
		};

		result = xrCreateActionSpace(session, &action_space_info, &hand_pose_spaces[hand]);
		if (!xr_check(instance, result, "failed to create hand %d pose space", hand))
			return false;
	}

	// Grabbing objects is not actually implemented in this demo, it only gives some  haptic feebdack.
	{
		XrActionCreateInfo action_info = {
		    .type = XR_TYPE_ACTION_CREATE_INFO,
		    .next = NULL,
		    .actionType = XR_ACTION_TYPE_FLOAT_INPUT,
		    .countSubactionPaths = HAND_COUNT,
		    .subactionPaths = hand_paths,
		};
		strcpy(action_info.actionName, "grabobjectfloat");
		strcpy(action_info.localizedActionName, "Grab Object");

		result = xrCreateAction(gameplay_actionset, &action_info, &grab_action_float);
		if (!xr_check(instance, result, "failed to create grab action"))
			return false;
	}

	{
		XrActionCreateInfo action_info = {
		    .type = XR_TYPE_ACTION_CREATE_INFO,
		    .next = NULL,
		    .actionType = XR_ACTION_TYPE_VIBRATION_OUTPUT,
		    .countSubactionPaths = HAND_COUNT,
		    .subactionPaths = hand_paths,
		};
		strcpy(action_info.actionName, "haptic");
		strcpy(action_info.localizedActionName, "Haptic Vibration");
		result = xrCreateAction(gameplay_actionset, &action_info, &haptic_action);
		if (!xr_check(instance, result, "failed to create haptic action"))
			return false;
	}


	// suggest actions for simple controller
	{
		XrPath interaction_profile_path;
		result = xrStringToPath(instance, "/interaction_profiles/khr/simple_controller",
		                        &interaction_profile_path);
		if (!xr_check(instance, result, "failed to get interaction profile"))
			return false;

		const XrActionSuggestedBinding bindings[] = {
		    {.action = hand_pose_action, .binding = grip_pose_path[HAND_LEFT_INDEX]},
		    {.action = hand_pose_action, .binding = grip_pose_path[HAND_RIGHT_INDEX]},
		    // boolean input select/click will be converted to float that is either 0 or 1
		    {.action = grab_action_float, .binding = select_click_path[HAND_LEFT_INDEX]},
		    {.action = grab_action_float, .binding = select_click_path[HAND_RIGHT_INDEX]},
		    {.action = haptic_action, .binding = haptic_path[HAND_LEFT_INDEX]},
		    {.action = haptic_action, .binding = haptic_path[HAND_RIGHT_INDEX]},
		};

		const XrInteractionProfileSuggestedBinding suggested_bindings = {
		    .type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING,
		    .next = NULL,
		    .interactionProfile = interaction_profile_path,
		    .countSuggestedBindings = sizeof(bindings) / sizeof(bindings[0]),
		    .suggestedBindings = bindings};

		xrSuggestInteractionProfileBindings(instance, &suggested_bindings);
		if (!xr_check(instance, result, "failed to suggest bindings"))
			return false;
	}

	// suggest actions for valve index controller
	{
		XrPath interaction_profile_path;
		result = xrStringToPath(instance, "/interaction_profiles/valve/index_controller",
		                        &interaction_profile_path);
		if (!xr_check(instance, result, "failed to get interaction profile"))
			return false;

		const XrActionSuggestedBinding bindings[] = {
		    {.action = hand_pose_action, .binding = grip_pose_path[HAND_LEFT_INDEX]},
		    {.action = hand_pose_action, .binding = grip_pose_path[HAND_RIGHT_INDEX]},
		    {.action = grab_action_float, .binding = trigger_value_path[HAND_LEFT_INDEX]},
		    {.action = grab_action_float, .binding = trigger_value_path[HAND_RIGHT_INDEX]},
		    {.action = haptic_action, .binding = haptic_path[HAND_LEFT_INDEX]},
		    {.action = haptic_action, .binding = haptic_path[HAND_RIGHT_INDEX]},
		};

		const XrInteractionProfileSuggestedBinding suggested_bindings = {
		    .type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING,
		    .next = NULL,
		    .interactionProfile = interaction_profile_path,
		    .countSuggestedBindings = sizeof(bindings) / sizeof(bindings[0]),
		    .suggestedBindings = bindings};

		xrSuggestInteractionProfileBindings(instance, &suggested_bindings);
		if (!xr_check(instance, result, "failed to suggest bindings"))
			return false;
	}

	XrSessionActionSetsAttachInfo actionset_attach_info = {
	    .type = XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO,
	    .next = NULL,
	    .countActionSets = 1,
	    .actionSets = &gameplay_actionset,
	};
	result = xrAttachSessionActionSets(session, &actionset_attach_info);
	if (!xr_check(instance, result, "failed to attach action set"))
		return false;

	return true;
}

bool
OpenXrContext::init()
{
	if (!init_instance()) {
		return false;
	}

	if (!init_system()) {
		return false;
	}

	if (!init_views()) {
		return false;
	}

	if (!init_graphics()) {
		return false;
	}

	if (!init_session()) {
		return false;
	}

	if (!init_space()) {
		return false;
	}

	if (!init_swapchains()) {
		return false;
	}

	if (!init_layers()) {
		return false;
	}

	if (!init_actions()) {
		return false;
	}

	// Set up rendering (compile shaders, ...) before starting the session
	if (init_gl(view_count, swapchain_lengths.data(), gl_rendering.framebuffers,
	            &gl_rendering.shader_program_id, &gl_rendering.VAO) != 0) {
		printf("OpenGl setup failed!\n");
		return false;
	}

	return true;
}

void
OpenXrContext::poll_sdl()
{
	SDL_Event sdl_event;
	while (SDL_PollEvent(&sdl_event)) {
		if (sdl_event.type == SDL_QUIT ||
		    (sdl_event.type == SDL_KEYDOWN && sdl_event.key.keysym.sym == SDLK_ESCAPE)) {
			printf("Requesting exit...\n");
			xrRequestExitSession(session);
		}
	}
}

bool
OpenXrContext::poll_events()
{
	XrResult result = XR_SUCCESS;

	// --- Handle runtime Events
	// we do this before xrWaitFrame() so we can go idle or
	// break out of the main render loop as early as possible and don't have to
	// uselessly render or submit one. Calling xrWaitFrame commits you to
	// calling xrBeginFrame eventually.
	XrEventDataBuffer runtime_event = {.type = XR_TYPE_EVENT_DATA_BUFFER, .next = NULL};
	XrResult poll_result = xrPollEvent(instance, &runtime_event);
	while (poll_result == XR_SUCCESS) {
		switch (runtime_event.type) {
		case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING: {
			XrEventDataInstanceLossPending* event = (XrEventDataInstanceLossPending*)&runtime_event;
			printf("EVENT: instance loss pending at %lu! Destroying instance.\n", event->lossTime);
			quit_mainloop = true;
			continue;
		}
		case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED: {
			XrEventDataSessionStateChanged* event = (XrEventDataSessionStateChanged*)&runtime_event;
			printf("EVENT: session state changed from %d to %d\n", state, event->state);
			state = event->state;

			/*
			 * react to session state changes, see OpenXR spec 9.3 diagram. What we need to react to:
			 *
			 * * READY -> xrBeginSession STOPPING -> xrEndSession (note that the same session can be
			 * restarted)
			 * * EXITING -> xrDestroySession (EXITING only happens after we went through STOPPING and
			 * called xrEndSession)
			 *
			 * After exiting it is still possible to create a new session but we don't do that here.
			 *
			 * * IDLE -> don't run render loop, but keep polling for events
			 * * SYNCHRONIZED, VISIBLE, FOCUSED -> run render loop
			 */
			switch (state) {
			// skip render loop, keep polling
			case XR_SESSION_STATE_MAX_ENUM: // must be a bug
			case XR_SESSION_STATE_IDLE:
			case XR_SESSION_STATE_UNKNOWN: {
				run_framecycle = false;

				break; // state handling switch
			}

			// do nothing, run render loop normally
			case XR_SESSION_STATE_FOCUSED:
			case XR_SESSION_STATE_SYNCHRONIZED:
			case XR_SESSION_STATE_VISIBLE: {
				run_framecycle = true;

				break; // state handling switch
			}

			// begin session and then run render loop
			case XR_SESSION_STATE_READY: {
				// start session only if it is not running, i.e. not when we already called xrBeginSession
				// but the runtime did not switch to the next state yet
				if (!session_running) {
					XrSessionBeginInfo session_begin_info = {.type = XR_TYPE_SESSION_BEGIN_INFO,
					                                         .next = NULL,
					                                         .primaryViewConfigurationType = view_type};
					result = xrBeginSession(session, &session_begin_info);
					if (!xr_check(instance, result, "Failed to begin session!"))
						return false;
					printf("Session started!\n");
					session_running = true;
				}
				// after beginning the session, run render loop
				run_framecycle = true;

				break; // state handling switch
			}

			// end session, skip render loop, keep polling for next state change
			case XR_SESSION_STATE_STOPPING: {
				// end session only if it is running, i.e. not when we already called xrEndSession but the
				// runtime did not switch to the next state yet
				if (session_running) {
					result = xrEndSession(session);
					if (!xr_check(instance, result, "Failed to end session!"))
						return false;
					session_running = false;
				}
				// after ending the session, don't run render loop
				run_framecycle = false;

				break; // state handling switch
			}

			// destroy session, skip render loop, exit render loop and quit
			case XR_SESSION_STATE_LOSS_PENDING:
			case XR_SESSION_STATE_EXITING:
				result = xrDestroySession(session);
				if (!xr_check(instance, result, "Failed to destroy session!"))
					return false;
				quit_mainloop = true;
				run_framecycle = false;

				break; // state handling switch
			}
			break; // session event handling switch
		}
		case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED: {
			printf("EVENT: interaction profile changed!\n");
			XrEventDataInteractionProfileChanged* event =
			    (XrEventDataInteractionProfileChanged*)&runtime_event;
			(void)event;

			XrInteractionProfileState state = {.type = XR_TYPE_INTERACTION_PROFILE_STATE};

			for (int i = 0; i < HAND_COUNT; i++) {
				XrResult res = xrGetCurrentInteractionProfile(session, hand_paths[i], &state);
				if (!xr_check(instance, res, "Failed to get interaction profile for %d", i))
					continue;

				XrPath prof = state.interactionProfile;

				uint32_t strl;
				char profile_str[XR_MAX_PATH_LENGTH];
				res = xrPathToString(instance, prof, XR_MAX_PATH_LENGTH, &strl, profile_str);
				if (!xr_check(instance, res, "Failed to get interaction profile path str for %d", i))
					continue;

				printf("Event: Interaction profile changed for %d: %s\n", i, profile_str);
			}
			break;
		}
		default: printf("Unhandled event (type %d)\n", runtime_event.type);
		}

		runtime_event.type = XR_TYPE_EVENT_DATA_BUFFER;
		poll_result = xrPollEvent(instance, &runtime_event);
	}
	if (poll_result == XR_EVENT_UNAVAILABLE) {
		// processed all events in the queue
	} else {
		printf("Failed to poll events!\n");
		return false;
	}

	return true;
}

bool
OpenXrContext::iterate()
{
	poll_sdl();
	poll_events();

	if (!run_framecycle) {
		return true;
	}

	// --- Wait for our turn to do head-pose dependent computation and render a frame
	XrFrameState frame_state = {.type = XR_TYPE_FRAME_STATE, .next = NULL};
	XrFrameWaitInfo frame_wait_info = {.type = XR_TYPE_FRAME_WAIT_INFO, .next = NULL};
	XrResult result = xrWaitFrame(session, &frame_wait_info, &frame_state);
	if (!xr_check(instance, result, "xrWaitFrame() was not successful, exiting..."))
		return false;

	// --- Create projection matrices and view matrices for each eye
	XrViewLocateInfo view_locate_info = {
	    .type = XR_TYPE_VIEW_LOCATE_INFO,
	    .next = NULL,
	    .viewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
	    .displayTime = frame_state.predictedDisplayTime,
	    .space = play_space,
	};

	XrViewState view_state = {.type = XR_TYPE_VIEW_STATE, .next = NULL};
	result =
	    xrLocateViews(session, &view_locate_info, &view_state, view_count, &view_count, views.data());
	if (!xr_check(instance, result, "Could not locate views"))
		return false;

	//! @todo Move this action processing to before xrWaitFrame, probably.
	const XrActiveActionSet active_actionsets[] = {
	    {.actionSet = gameplay_actionset, .subactionPath = XR_NULL_PATH}};

	XrActionsSyncInfo actions_sync_info = {
	    .type = XR_TYPE_ACTIONS_SYNC_INFO,
	    .countActiveActionSets = sizeof(active_actionsets) / sizeof(active_actionsets[0]),
	    .activeActionSets = active_actionsets,
	};
	result = xrSyncActions(session, &actions_sync_info);
	xr_check(instance, result, "failed to sync actions!");

	// query each value / location with a subaction path != XR_NULL_PATH
	// resulting in individual values per hand/.
	XrActionStateFloat grab_value[HAND_COUNT];
	XrSpaceLocation hand_locations[HAND_COUNT];

	for (int i = 0; i < HAND_COUNT; i++) {
		XrActionStatePose hand_pose_state = {.type = XR_TYPE_ACTION_STATE_POSE, .next = NULL};
		{
			XrActionStateGetInfo get_info = {
			    .type = XR_TYPE_ACTION_STATE_GET_INFO,
			    .next = NULL,
			    .action = hand_pose_action,
			    .subactionPath = hand_paths[i],
			};
			result = xrGetActionStatePose(session, &get_info, &hand_pose_state);
			xr_check(instance, result, "failed to get pose value!");
		}

		hand_locations[i].type = XR_TYPE_SPACE_LOCATION;
		hand_locations[i].next = NULL;

		result = xrLocateSpace(hand_pose_spaces[i], play_space, frame_state.predictedDisplayTime,
		                       &hand_locations[i]);
		xr_check(instance, result, "failed to locate space %d!", i);

		grab_value[i].type = XR_TYPE_ACTION_STATE_FLOAT;
		grab_value[i].next = NULL;
		{
			XrActionStateGetInfo get_info = {
			    .type = XR_TYPE_ACTION_STATE_GET_INFO,
			    .next = NULL,
			    .action = grab_action_float,
			    .subactionPath = hand_paths[i],
			};

			result = xrGetActionStateFloat(session, &get_info, &grab_value[i]);
			xr_check(instance, result, "failed to get grab value!");
		}

		if (grab_value[i].isActive && grab_value[i].currentState > 0.75) {
			XrHapticVibration vibration = {
			    .type = XR_TYPE_HAPTIC_VIBRATION,
			    .next = NULL,
			    .duration = XR_MIN_HAPTIC_DURATION,
			    .frequency = XR_FREQUENCY_UNSPECIFIED,
			    .amplitude = 0.5,
			};

			XrHapticActionInfo haptic_action_info = {
			    .type = XR_TYPE_HAPTIC_ACTION_INFO,
			    .next = NULL,
			    .action = haptic_action,
			    .subactionPath = hand_paths[i],
			};
			result = xrApplyHapticFeedback(session, &haptic_action_info,
			                               (const XrHapticBaseHeader*)&vibration);
			xr_check(instance, result, "failed to apply haptic feedback!");
			// printf("Sent haptic output to hand %d\n", i);
		}
	};

	// --- Begin frame
	XrFrameBeginInfo frame_begin_info = {.type = XR_TYPE_FRAME_BEGIN_INFO, .next = NULL};

	result = xrBeginFrame(session, &frame_begin_info);
	if (!xr_check(instance, result, "failed to begin frame!"))
		return false;


	// render each eye and fill projection_views with the result
	for (uint32_t i = 0; i < view_count; i++) {

		if (!frame_state.shouldRender) {
			printf("shouldRender = false, Skipping rendering work\n");
			continue;
		}

		glm::mat4 projection_matrix =
		    create_projection_from_fov(views[i].fov, gl_rendering.near_z, gl_rendering.far_z);

		glm::mat4 view_matrix = create_view_matrix(xrVecToGlm(views[i].pose.position),
		                                           xrQuatToGlm(views[i].pose.orientation));

		XrSwapchainImageAcquireInfo acquire_info = {.type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO,
		                                            .next = NULL};
		uint32_t acquired_index;
		result = xrAcquireSwapchainImage(swapchains[i], &acquire_info, &acquired_index);
		if (!xr_check(instance, result, "failed to acquire swapchain image!"))
			return false;

		XrSwapchainImageWaitInfo wait_info = {
		    .type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO, .next = NULL, .timeout = 1000};
		result = xrWaitSwapchainImage(swapchains[i], &wait_info);
		if (!xr_check(instance, result, "failed to wait for swapchain image!"))
			return false;

		uint32_t depth_acquired_index = UINT32_MAX;
		if (depth.supported) {
			XrSwapchainImageAcquireInfo depth_acquire_info = {
			    .type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO, .next = NULL};
			result =
			    xrAcquireSwapchainImage(depth_swapchains[i], &depth_acquire_info, &depth_acquired_index);
			if (!xr_check(instance, result, "failed to acquire swapchain image!"))
				return false;

			XrSwapchainImageWaitInfo depth_wait_info = {
			    .type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO, .next = NULL, .timeout = 1000};
			result = xrWaitSwapchainImage(depth_swapchains[i], &depth_wait_info);
			if (!xr_check(instance, result, "failed to wait for swapchain image!"))
				return false;
		}

		projection_views[i].pose = views[i].pose;
		projection_views[i].fov = views[i].fov;

		GLuint depth_image = depth.supported ? depth_images[i][depth_acquired_index].image : 0;

		int w = viewconfig_views[i].recommendedImageRectWidth;
		int h = viewconfig_views[i].recommendedImageRectHeight;

		render_frame(w, h, gl_rendering.shader_program_id, gl_rendering.VAO,
		             frame_state.predictedDisplayTime, i, hand_locations, projection_matrix,
		             view_matrix, gl_rendering.framebuffers[i][acquired_index],
		             images[i][acquired_index].image, depth.supported, depth_image);

		XrSwapchainImageReleaseInfo release_info = {.type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO,
		                                            .next = NULL};
		result = xrReleaseSwapchainImage(swapchains[i], &release_info);
		if (!xr_check(instance, result, "failed to release swapchain image!"))
			return false;

		if (depth.supported) {
			XrSwapchainImageReleaseInfo depth_release_info = {
			    .type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO, .next = NULL};
			result = xrReleaseSwapchainImage(depth_swapchains[i], &depth_release_info);
			if (!xr_check(instance, result, "failed to release swapchain image!"))
				return false;
		}
	}

	XrCompositionLayerProjection projection_layer = {
	    .type = XR_TYPE_COMPOSITION_LAYER_PROJECTION,
	    .next = NULL,
	    .layerFlags = 0,
	    .space = play_space,
	    .viewCount = view_count,
	    .views = projection_views.data(),
	};

	uint32_t submitted_layer_count = 1;
	const XrCompositionLayerBaseHeader* submitted_layers[1] = {
	    (const XrCompositionLayerBaseHeader*)&projection_layer,
	};

	if ((view_state.viewStateFlags & XR_VIEW_STATE_ORIENTATION_VALID_BIT) == 0) {
		printf("submitting 0 layers because orientation is invalid\n");
		submitted_layer_count = 0;
	}

	if (!frame_state.shouldRender) {
		printf("submitting 0 layers because shouldRender = false\n");
		submitted_layer_count = 0;
	}

	XrFrameEndInfo frameEndInfo = {
	    .type = XR_TYPE_FRAME_END_INFO,
	    .next = NULL,
	    .displayTime = frame_state.predictedDisplayTime,
	    .environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE,
	    .layerCount = submitted_layer_count,
	    .layers = submitted_layers,
	};
	result = xrEndFrame(session, &frameEndInfo);
	if (!xr_check(instance, result, "failed to end frame!"))
		return false;

	return true;
}

bool
OpenXrContext::loop()
{
	while (!quit_mainloop) {
		if (!iterate()) {
			return false;
		}
	}
	return true;
}

// =============================================================================
// OpenGL rendering code
// =============================================================================

static SDL_Window* desktop_window;
static SDL_GLContext gl_context;

#ifndef GLAPIENTRY
#define GLAPIENTRY
#endif

void GLAPIENTRY
MessageCallback(GLenum source,
                GLenum type,
                GLuint id,
                GLenum severity,
                GLsizei length,
                const GLchar* message,
                const void* userParam)
{
	fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
	        (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""), type, severity, message);
}

bool
init_sdl_window(Display** xDisplay,
                uint32_t* visualid,
                GLXFBConfig* glxFBConfig,
                GLXDrawable* glxDrawable,
                GLXContext* glxContext,
                int w,
                int h)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("Unable to initialize SDL");
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 0);


	/* Create our window centered at half the VR resolution */
	desktop_window =
	    SDL_CreateWindow("OpenXR Example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w / 2,
	                     h / 2, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!desktop_window) {
		printf("Unable to create window");
		return false;
	}

	gl_context = SDL_GL_CreateContext(desktop_window);

	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, 0);

	SDL_GL_SetSwapInterval(0);

	// HACK? OpenXR wants us to report these values, so "work around" SDL a
	// bit and get the underlying glx stuff. Does this still work when e.g.
	// SDL switches to xcb?
	*xDisplay = XOpenDisplay(NULL);
	*glxContext = glXGetCurrentContext();
	*glxDrawable = glXGetCurrentDrawable();

	return true;
}


std::string vertexshader = R"(
	#version 460 core

	layout(location = 0) in vec3 in_position;
	layout(location = 1) in vec2 in_color;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

	layout(location = 0) out vec2 out_color;

	void main() {
		gl_Position = projection * view * model * vec4(in_position, 1.0);
		out_color = in_color;
	}
)";

std::string fragmentshader = R"(
	#version 460 core

	uniform vec3 uniform_color;

	layout(location = 0) in vec2 vertex_color;
	layout(location = 0) out vec4 frag_color;

	void main() {
		if (length(uniform_color) > 0.0f) {
			frag_color = vec4(uniform_color, 1.0);
		} else {
			frag_color = vec4(vertex_color, 1.0, 1.0);
		}
	}
)";


int
init_gl(uint32_t view_count,
        uint32_t* swapchain_lengths,
        std::vector<std::vector<GLuint>>& framebuffers,
        GLuint* shader_program_id,
        GLuint* VAO)
{

	/* Allocate resources that we use for our own rendering.
	 * We will bind framebuffers to the runtime provided textures for rendering.
	 * For this, we create one framebuffer per OpenGL texture.
	 * This is not mandated by OpenXR, other ways to render to textures will work too.
	 */
	for (uint32_t i = 0; i < view_count; i++) {
		std::vector<GLuint> view_framebuffers(swapchain_lengths[i]);
		glGenFramebuffers(swapchain_lengths[i], view_framebuffers.data());
		framebuffers.push_back(view_framebuffers);
	}

	GLuint vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertex_shader_source[1] = {vertexshader.c_str()};
	glShaderSource(vertex_shader_id, 1, vertex_shader_source, NULL);
	glCompileShader(vertex_shader_id);
	int vertex_compile_res;
	glGetShaderiv(vertex_shader_id, GL_COMPILE_STATUS, &vertex_compile_res);
	if (!vertex_compile_res) {
		char info_log[512];
		glGetShaderInfoLog(vertex_shader_id, 512, NULL, info_log);
		printf("Vertex Shader failed to compile: %s\n", info_log);
		return 1;
	} else {
		printf("Successfully compiled vertex shader!\n");
	}

	GLuint fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fragment_shader_source[1] = {fragmentshader.c_str()};
	glShaderSource(fragment_shader_id, 1, fragment_shader_source, NULL);
	glCompileShader(fragment_shader_id);
	int fragment_compile_res;
	glGetShaderiv(fragment_shader_id, GL_COMPILE_STATUS, &fragment_compile_res);
	if (!fragment_compile_res) {
		char info_log[512];
		glGetShaderInfoLog(fragment_shader_id, 512, NULL, info_log);
		printf("Fragment Shader failed to compile: %s\n", info_log);
		return 1;
	} else {
		printf("Successfully compiled fragment shader!\n");
	}

	*shader_program_id = glCreateProgram();
	glAttachShader(*shader_program_id, vertex_shader_id);
	glAttachShader(*shader_program_id, fragment_shader_id);
	glLinkProgram(*shader_program_id);
	GLint shader_program_res;
	glGetProgramiv(*shader_program_id, GL_LINK_STATUS, &shader_program_res);
	if (!shader_program_res) {
		char info_log[512];
		glGetProgramInfoLog(*shader_program_id, 512, NULL, info_log);
		printf("Shader Program failed to link: %s\n", info_log);
		return 1;
	} else {
		printf("Successfully linked shader program!\n");
	}

	glDeleteShader(vertex_shader_id);
	glDeleteShader(fragment_shader_id);

	// Position (x, y, z) Color (r, g)
	// clang-format off
	const GLfloat vertices[] = {
		-0.5f,  0.5f,  0.5f, 0.0f, 0.0f,
		 0.5f,  0.5f,  0.5f, 0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f, 1.0f, 0.0f,
		 0.5f, -0.5f,  0.5f, 1.0f, 1.0f,
		 0.5f, -0.5f, -0.5f, 0.0f, 0.0f,
		 0.5f,  0.5f,  0.5f, 0.0f, 1.0f,
		 0.5f,  0.5f, -0.5f, 1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f, 1.0f, 1.0f,
		-0.5f,  0.5f, -0.5f, 0.0f, 0.0f,
		-0.5f, -0.5f,  0.5f, 0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f, 1.0f, 0.0f,
		 0.5f, -0.5f, -0.5f, 1.0f, 1.0f,
		-0.5f,  0.5f, -0.5f, 0.0f, 0.0f,
		 0.5f,  0.5f, -0.5f, 0.0f, 1.0f,
	};
	// clang-format on
	GLuint VBOs[1];
	glGenBuffers(1, VBOs);

	glGenVertexArrays(1, VAO);

	glBindVertexArray(*VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBOs[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glEnable(GL_DEPTH_TEST);

	return 0;
}

static void
render_controller(glm::vec3 position,
                  glm::quat orientation,
                  glm::vec3 scale_vec,
                  int uniform_location)
{
	glm::mat4 model = glm::translate(position) * glm::toMat4(orientation) * glm::scale(scale_vec);
	glUniformMatrix4fv(uniform_location, 1, GL_FALSE, glm::value_ptr(model));

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 36);
}

void
render_cube(const glm::vec3& position, float cube_size, float angle_degrees, int uniform_location)
{
	glm::mat4 model = glm::translate(position);
	model = glm::scale(model, glm::vec3(cube_size / 2.0f));
	model = glm::rotate(model, glm::radians(angle_degrees), glm::vec3(0.0f, 1.0f, 0.0f));

	glUniformMatrix4fv(uniform_location, 1, GL_FALSE, glm::value_ptr(model));
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 36);
}



void
render_frame(int w,
             int h,
             GLuint shader_program_id,
             GLuint VAO,
             XrTime predictedDisplayTime,
             int view_index,
             XrSpaceLocation* hand_locations,
             const glm::mat4& projection_matrix,
             const glm::mat4& view_matrix,
             GLuint framebuffer,
             GLuint image,
             bool depth_supported,
             GLuint depthbuffer)
{
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	glViewport(0, 0, w, h);
	glScissor(0, 0, w, h);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, image, 0);
	if (depth_supported) {
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthbuffer, 0);
	} else {
		// TODO: need a depth attachment for depth test when rendering to fbo
	}

	glClearColor(.0f, 0.0f, 0.2f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	glUseProgram(shader_program_id);
	glBindVertexArray(VAO);

	int modelLoc = glGetUniformLocation(shader_program_id, "model");
	int colorLoc = glGetUniformLocation(shader_program_id, "uniform_color");
	int viewLoc = glGetUniformLocation(shader_program_id, "view");
	int projLoc = glGetUniformLocation(shader_program_id, "projection");

	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view_matrix));
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection_matrix));

	// When black, vertex color is used instead.
	glUniform3f(colorLoc, 0.0, 0.0, 0.0);

	double display_time_seconds = ((double)predictedDisplayTime) / (1000. * 1000. * 1000.);
	const float rotations_per_sec = .25;
	float angle = ((long)(display_time_seconds * 360. * rotations_per_sec)) % 360;

	float dist = 1.5f;
	float height = 0.5f;
	render_cube(glm::vec3(0, height, -dist), .33f, angle, modelLoc);
	render_cube(glm::vec3(0, height, dist), .33f, angle, modelLoc);
	render_cube(glm::vec3(dist, height, 0), .33f, angle, modelLoc);
	render_cube(glm::vec3(-dist, height, 0), .33f, angle, modelLoc);

	// render controllers
	for (int hand = 0; hand < 2; hand++) {
		if (hand == 0) {
			glUniform3f(colorLoc, 1.0, 0.5, 0.5);
		} else {
			glUniform3f(colorLoc, 0.5, 1.0, 0.5);
		}

		bool hand_location_valid =
		    //(spaceLocation[hand].locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT) != 0 &&
		    (hand_locations[hand].locationFlags & XR_SPACE_LOCATION_ORIENTATION_VALID_BIT) != 0;

		// draw a block at the controller pose
		if (!hand_location_valid)
			continue;

		glm::vec3 scale(.05f, .05f, .2f);
		render_controller(xrVecToGlm(hand_locations[hand].pose.position),
		                  xrQuatToGlm(hand_locations[hand].pose.orientation), scale, modelLoc);
	}


	// blit left eye to desktop window
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	if (view_index == 0) {
		glBlitNamedFramebuffer((GLuint)framebuffer,             // readFramebuffer
		                       (GLuint)0,                       // backbuffer     // drawFramebuffer
		                       (GLint)0,                        // srcX0
		                       (GLint)0,                        // srcY0
		                       (GLint)w,                        // srcX1
		                       (GLint)h,                        // srcY1
		                       (GLint)0,                        // dstX0
		                       (GLint)0,                        // dstY0
		                       (GLint)w / 2,                    // dstX1
		                       (GLint)h / 2,                    // dstY1
		                       (GLbitfield)GL_COLOR_BUFFER_BIT, // mask
		                       (GLenum)GL_LINEAR);              // filter

		SDL_GL_SwapWindow(desktop_window);
	}
}

int
main(int argc, char** argv)
{
	print_api_layers();

	OpenXrContext context;
	if (context.init()) {
		context.loop();
	}
}
