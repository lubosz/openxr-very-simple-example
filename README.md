# OpenXR Very Simple Example

Very OpenXR, very example.

A simplified C++ and GLM fork of Christoph Haag's [OpenXR Simple Example](https://gitlab.freedesktop.org/monado/demos/openxr-simple-example).

This example uses OpenGL, if you are interested in Vulkan, check out [xrgears](https://gitlab.freedesktop.org/monado/demos/xrgears).

# Compiling

```
cmake -G Ninja -B build -DCMAKE_BUILD_TYPE=Release
ninja -C build
```

# Running

The `XR_RUNTIME_JSON` variable has to be set for the loader to know where to look for the runtime, e.g.:
```
XR_RUNTIME_JSON=~/monado/build/openxr_monado-dev.json
```
or
```
XR_RUNTIME_JSON=~/.local/share/Steam/steamapps/common/SteamVR/steamxr_linux64.json
```

# License

This code is licensed under the SPDX-License-Identifier: BSL-1.0. See [LICENSE](LICENSE).

In addition to that, I give permission to anyone use this code under any other license. Create a pull request or contact me if you need to officially add another license.
